1.  - user-crud\database\File SQL\tms.sql => file sql để chạy
	3.1) composer install
	3.2) php artisan migrate
	3.3) php artisan db:seed)
2. API  - Dùng tool PostMan để test API.
	GET : http://localhost:8000/users - get all users
	GET : http://localhost:8000/users/1/edit - get profile user
	PUT : http://localhost:8000/users/1 (Body chonj x-www-form-urlencoded)
	POST : http://localhost:8000/users (Body form-data)
	DELETE : http://localhost:8000/items/1